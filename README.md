# Stripe Service

## Container Image

This repo packages itself into an image you can use with K8 or Docker (sample config below). You can use: `registry.gitlab.com/theplacelab/service/stripe`

### Sample K8 Config

```yml
apiVersion: v1
kind: Service
metadata:
  name: stripe-helper
spec:
  ports:
    - port: 80
      targetPort: 8888
  selector:
    app: stripehelper
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: stripe-helper
spec:
  replicas: 1
  selector:
    matchLabels:
      app: stripehelper
  strategy: {}
  template:
    metadata:
      labels:
        app: stripehelper
    spec:
      containers:
        - name: stripe-helper
          image: registry.gitlab.com/theplacelab/service/stripe
          imagePullPolicy: Always
          resources: {}
          env:
            - name: STRIPE_SECRET_API_KEY
              value: sk_********************
          ports:
            - containerPort: 8888
      restartPolicy: Always
      serviceAccountName: ""
status: {}
```

### Licence / Credits

- [MIT License](/LICENSE)
