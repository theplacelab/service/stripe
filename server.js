"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const express = require("express");
const cors = require("cors");
const routeStripe = require("./src/routeStripe.js");
const app = express();
const fs = require("fs");
const https = require("https");
const port = process.env.PORT || 8888;
const token = require("./src/middleware.js");

console.log(`\n--------------------------------------`);
console.log(`Stripe Payment Support Server`);
console.log(`Port: ${port}`);
console.log(`--------------------------------------`);

app.use(cors());
app.use(express.json());
app.use(express.static("public"));
app.engine("html", require("ejs").renderFile);

try {
  let certificate = fs.readFileSync("/cert/fullchain.pem");
  let privateKey = fs.readFileSync("/cert/privkey.pem");
  https
    .createServer(
      {
        key: privateKey,
        cert: certificate
      },
      app
    )
    .listen(port);
  console.log("SSL: ENABLED");
} catch {
  console.log("SSL: DISABLED - CERTIFICATE NOT FOUND");
  app.listen(port);
}

app.post("/charge", token.validate, (req, res) => {
  routeStripe.charge(req, res);
});

// Fallthrough
app.use(function (req, res) {
  console.log(`REJECTING: ${req.method}:${req.originalUrl}`);
  res.sendStatus(200);
});
