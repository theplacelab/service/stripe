const stripe = require("stripe")(process.env.STRIPE_SECRET_API_KEY);
const { v4: uuidv4 } = require("uuid");
const axios = require("axios");
let jwt = require("jsonwebtoken");

const logError = (msg, err) => {
  console.error(msg);
  console.error("-- Got -----------------------");
  console.error(`${err?.response?.status} ${err?.response?.statusText}`);
  console.error(err?.response?.data);
  console.error("-------------------------------");
};

module.exports = {
  charge: async (req, res) => {
    try {
      debugger;
      const {
        amount,
        source,
        receipt_email,
        currency,
        description,
        product_id,
        metadata
      } = req.body;

      // Make stripe charge
      let charge = { id: "free" };
      if (parseInt(amount, 10) !== 0) {
        charge = await stripe.charges.create({
          amount,
          currency: currency || "usd",
          source,
          description,
          receipt_email
        });
        if (!charge) throw new Error("charge unsuccessful");
      }

      // If ok, register purchase with shop API
      if (process.env.SHOP_API) {
        debugger;
        const order_id = uuidv4();
        axios({
          url: `${process.env.SHOP_API}/rpc/purchase`,
          method: "POST",
          data: {
            order_id,
            product_id,
            charge: {
              ...charge,
              metadata: metadata,
              billing_details: "REDACTED",
              payment_method_details: "REDACTED"
            }
          },
          headers: {
            Authorization: `Bearer ${jwt.sign(
              {
                role: "service",
                uuid: req.token.uuid, // Original user making purchase
                exp: Math.floor(Date.now() / 1000) + 2 * 60 // 2 min token
              },
              `${process.env.JWT_SECRET}`
            )}`,
            "Content-Type": "application/json; charset=utf-8"
          }
        })
          .then(() => {
            res.status(200).json({
              message: "success",
              order_id,
              charge
            });
          })
          .catch((err) => {
            console.error(err);
            logError(
              "ERROR: Charge ran, but could not register purchase with shop",
              err
            );
            res.status(err.response.status).json({
              message: "Could not register purchase"
            });
          });
      } else {
        res.status(200).json({
          message: "success",
          order_id: null,
          charge
        });
      }
    } catch (err) {
      console.log(err);
      logError("ERROR: Failed running charge", err);
      res.status(500).json({
        message: "Failed running charge"
      });
    }
  }
};
